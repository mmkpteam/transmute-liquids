package com.mrpenguin.transmuteliquids.proxy;

import java.io.File;

/**
 * Copyright (c) 2014 MrPenguin
 * All rights reserved.
 *
 * This program and the accompanying materials are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 *
 * This class was made by MrPenguin and is distributed as a part of the Transmute Liquids mod.
 * Transmute Liquids is a derivative work on Thaumcraft 4 (c) Azanor 2012.
 * http://www.minecraftforum.net/topic/2011841-thaumcraft-41114-updated-2052014/
 */

public interface IProxy {
    public abstract void registerBlocks();

    public abstract void registerTileEntities();

    //public abstract void registerEntities();

    public abstract void registerEvents();

    public abstract void registerItems();

    //public abstract void registerPlayerEvents();

    //public abstract void initPacketHandler();

    //public abstract void postInitPacketHandler();

    public abstract void registerRecipes();
}
