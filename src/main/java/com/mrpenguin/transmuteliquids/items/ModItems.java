package com.mrpenguin.transmuteliquids.items;

/**
 * Copyright (c) 2014 MrPenguin
 * All rights reserved.
 *
 * This program and the accompanying materials are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 *
 * This class was made by MrPenguin and is distributed as a part of the Transmute Liquids mod.
 * Transmute Liquids is a derivative work on Thaumcraft 4 (c) Azanor 2012.
 * http://www.minecraftforum.net/topic/2011841-thaumcraft-41114-updated-2052014/
 */

import com.mrpenguin.transmuteliquids.blocks.fluids.ModFluids;
import com.mrpenguin.transmuteliquids.items.buckets.*;
import com.mrpenguin.transmuteliquids.items.ingots.*;
import com.mrpenguin.transmuteliquids.lib.Forestry;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.FluidContainerRegistry;
import net.minecraftforge.fluids.FluidRegistry;

public class ModItems {

    public static Item moltenAirShardBucket = new ItemMoltenAirShardBucket(ModFluids.moltenAirShardBlock);
    public static Item moltenFireShardBucket = new ItemMoltenFireShardBucket(ModFluids.moltenFireShardBlock);
    public static Item moltenWaterShardBucket = new ItemMoltenWaterShardBucket(ModFluids.moltenWaterShardBlock);
    public static Item moltenEarthShardBucket = new ItemMoltenEarthShardBucket(ModFluids.moltenEarthShardBlock);
    public static Item moltenOrderShardBucket = new ItemMoltenOrderShardBucket(ModFluids.moltenOrderShardBlock);
    public static Item moltenEntropyShardBucket = new ItemMoltenEntropyBucket(ModFluids.moltenEntropyShardBlock);

    public static Item airIngot = new ItemAirIngot();
    public static Item fireIngot = new ItemFireIngot();
    public static Item waterIngot = new ItemWaterIngot();
    public static Item earthIngot = new ItemEarthIngot();
    public static Item orderIngot = new ItemOrderIngot();
    public static Item entropyIngot = new ItemEntropyIngot();

    public static void registerItems() {
        moltenAirShardBucket.setUnlocalizedName("moltenAirShardBucket").setContainerItem(Items.bucket);
        GameRegistry.registerItem(moltenAirShardBucket, "moltenAirShardBucket");
        FluidContainerRegistry.registerFluidContainer(FluidRegistry.getFluidStack(ModFluids.moltenAirShard.getName(), FluidContainerRegistry.BUCKET_VOLUME), new ItemStack(moltenAirShardBucket, 1, 0), new ItemStack(Items.bucket, 1, 0));

        moltenFireShardBucket.setUnlocalizedName("moltenFireShardBucket").setContainerItem(Items.bucket);
        GameRegistry.registerItem(moltenFireShardBucket, "moltenFireShardBucket");
        FluidContainerRegistry.registerFluidContainer(FluidRegistry.getFluidStack(ModFluids.moltenFireShard.getName(), FluidContainerRegistry.BUCKET_VOLUME), new ItemStack(moltenFireShardBucket, 1, 0), new ItemStack(Items.bucket, 1, 0));

        moltenWaterShardBucket.setUnlocalizedName("moltenWaterShardBucket").setContainerItem(Items.bucket);
        GameRegistry.registerItem(moltenWaterShardBucket, "moltenWaterShardBucket");
        FluidContainerRegistry.registerFluidContainer(FluidRegistry.getFluidStack(ModFluids.moltenWaterShard.getName(), FluidContainerRegistry.BUCKET_VOLUME), new ItemStack(moltenWaterShardBucket, 1, 0), new ItemStack(Items.bucket, 1, 0));

        moltenEarthShardBucket.setUnlocalizedName("moltenEarthShardBucket").setContainerItem(Items.bucket);
        GameRegistry.registerItem(moltenEarthShardBucket, "moltenEarthShardBucket");
        FluidContainerRegistry.registerFluidContainer(FluidRegistry.getFluidStack(ModFluids.moltenEarthShard.getName(), FluidContainerRegistry.BUCKET_VOLUME), new ItemStack(moltenEarthShardBucket, 1, 0), new ItemStack(Items.bucket, 1, 0));

        moltenOrderShardBucket.setUnlocalizedName("moltenOrderShardBucket").setContainerItem(Items.bucket);
        GameRegistry.registerItem(moltenOrderShardBucket, "moltenOrderShardBucket");
        FluidContainerRegistry.registerFluidContainer(FluidRegistry.getFluidStack(ModFluids.moltenOrderShard.getName(), FluidContainerRegistry.BUCKET_VOLUME), new ItemStack(moltenOrderShardBucket, 1, 0), new ItemStack(Items.bucket, 1, 0));

        moltenEntropyShardBucket.setUnlocalizedName("moltenEntropyShardBucket").setContainerItem(Items.bucket);
        GameRegistry.registerItem(moltenEntropyShardBucket, "moltenEntropyShardBucket");
        FluidContainerRegistry.registerFluidContainer(FluidRegistry.getFluidStack(ModFluids.moltenEntropyShard.getName(), FluidContainerRegistry.BUCKET_VOLUME), new ItemStack(moltenEntropyShardBucket, 1, 0), new ItemStack(Items.bucket, 1, 0));

        airIngot.setUnlocalizedName("airIngot");
        fireIngot.setUnlocalizedName("fireIngot");
        waterIngot.setUnlocalizedName("waterIngot");
        earthIngot.setUnlocalizedName("earthIngot");
        orderIngot.setUnlocalizedName("orderIngot");
        entropyIngot.setUnlocalizedName("entropyIngot");

        GameRegistry.registerItem(airIngot, "airIngot");
        GameRegistry.registerItem(fireIngot, "fireIngot");
        GameRegistry.registerItem(waterIngot, "waterIngot");
        GameRegistry.registerItem(earthIngot, "earthIngot");
        GameRegistry.registerItem(orderIngot, "orderIngot");
        GameRegistry.registerItem(entropyIngot, "entropyIngot");
    }

    /*private static boolean registered = false;
    public final Item item;
    private final String internalName;

    Items(String internalName, Item item, CreativeTabs creativeTabs) {
        this.internalName = internalName;
        this.item = item.setTextureName(Reference.MOD_ID + ":" + internalName);
        item.setUnlocalizedName(Reference.MOD_ID + "." + internalName);
        item.setCreativeTab(creativeTabs);
    }

    public static void registerAll() {
        if(registered) {
            return;
        }
        for(Items i : Items.values()) {
            i.register();
        }
    }

    private void register() {
        GameRegistry.registerItem(item, internalName);
    }

    public String getInternalName() {
        return internalName;
    }

    public String getStatName() {
        return StatCollector.translateToLocal(item.getUnlocalizedName());
    }

    public ItemStack getStack(int meta, int size) {
        return new ItemStack(item, size, meta);
    }*/
}
