package com.mrpenguin.transmuteliquids.items.buckets;

/**
 * Copyright (c) 2014 MrPenguin
 * All rights reserved.
 *
 * This program and the accompanying materials are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 *
 * This class was made by MrPenguin and is distributed as a part of the Transmute Liquids mod.
 * Transmute Liquids is a derivative work on Thaumcraft 4 (c) Azanor 2012.
 * http://www.minecraftforum.net/topic/2011841-thaumcraft-41114-updated-2052014/
 */

import com.mrpenguin.transmuteliquids.creativetab.ModCreativeTab;
import net.minecraft.block.Block;
import net.minecraft.item.ItemBucket;

public class ItemMoltenEarthShardBucket extends ItemBucket{

    private Block fluid;

    public ItemMoltenEarthShardBucket(Block fluid) {
        super(fluid);
        setTextureName("transmuteliquids:buckets/moltenEarthShardBucket");
        this.fluid = fluid;
        setCreativeTab(ModCreativeTab.Transmute_Liquids);

    }
}
