package com.mrpenguin.transmuteliquids.lib;

/**
 * Copyright (c) 2014 MrPenguin
 * All rights reserved.
 *
 * This program and the accompanying materials are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 *
 * This class was made by MrPenguin and is distributed as a part of the Transmute Liquids mod.
 * Transmute Liquids is a derivative work on Thaumcraft 4 (c) Azanor 2012.
 * http://www.minecraftforum.net/topic/2011841-thaumcraft-41114-updated-2052014/
 */


import thaumcraft.api.ThaumcraftApi;
import thaumcraft.api.aspects.Aspect;
import thaumcraft.api.aspects.AspectList;
import thaumcraft.api.crafting.CrucibleRecipe;
import thaumcraft.api.research.ResearchPage;

import cpw.mods.fml.common.registry.GameRegistry;

import net.minecraft.item.ItemStack;

public class Buildcraft {
    public static final String buildcraftEnergy = "BuildCraft|Energy";

    public static final String transmuteOil = "transmuteOil";
    public static final String transmuteFuel = "transmuteFuel";

    public static final String transmuteOilText = "transmuteOilText";
    public static final String transmuteFuelText = "transmuteFuelText";

    public static final ItemStack bucketOil = GameRegistry.findItemStack(buildcraftEnergy, "bucketOil", 1);
    public static final ItemStack waxCapsuleOil = GameRegistry.findItemStack(Forestry.Forestry, "waxCapsuleOil", 1);
    public static final ItemStack refractoryOil = GameRegistry.findItemStack(Forestry.Forestry, "refractoryOil", 1);
    public static final ItemStack canOil = GameRegistry.findItemStack(Forestry.Forestry, "canOil", 1);

    public static final ItemStack bucketFuel = GameRegistry.findItemStack(buildcraftEnergy, "bucketFuel", 1);
    public static final ItemStack waxCapsuleFuel = GameRegistry.findItemStack(Forestry.Forestry, "waxCapsuleFuel", 1);
    public static final ItemStack refractoryFuel = GameRegistry.findItemStack(Forestry.Forestry, "refractoryFuel", 1);
    public static final ItemStack canFuel = GameRegistry.findItemStack(Forestry.Forestry, "canFuel", 1);

    public static final AspectList oilAspectList = new AspectList().add(Aspect.LIFE, 6).add(Aspect.EXCHANGE, 3);
    public static final AspectList fuelAspectList = new AspectList().add(Aspect.LIFE, 9).add(Aspect.TOOL, 4).add(Aspect.EXCHANGE, 3);

    public static final CrucibleRecipe bucketOilRecipe = ThaumcraftApi.addCrucibleRecipe(transmuteOil, bucketOil, TransmuteLiquids.bucket, oilAspectList);
    public static final CrucibleRecipe waxCapsuleOilRecipe = ThaumcraftApi.addCrucibleRecipe(transmuteOil, waxCapsuleOil, Forestry.waxCapsule, oilAspectList);
    public static final CrucibleRecipe refractoryOilRecipe = ThaumcraftApi.addCrucibleRecipe(transmuteOil, refractoryOil, Forestry.refractoryEmpty, oilAspectList);
    public static final CrucibleRecipe canOilRecipe = ThaumcraftApi.addCrucibleRecipe(transmuteOil, canOil, Forestry.canEmpty, oilAspectList);

    public static final CrucibleRecipe bucketFuelRecipe = ThaumcraftApi.addCrucibleRecipe(transmuteFuel, bucketFuel, TransmuteLiquids.bucket, fuelAspectList);
    public static final CrucibleRecipe waxCapsuleFuelRecipe = ThaumcraftApi.addCrucibleRecipe(transmuteFuel, waxCapsuleFuel, Forestry.waxCapsule, fuelAspectList);
    public static final CrucibleRecipe refractoryFuelRecipe = ThaumcraftApi.addCrucibleRecipe(transmuteFuel, refractoryFuel, Forestry.refractoryEmpty, fuelAspectList);
    public static final CrucibleRecipe canFuelRecipe = ThaumcraftApi.addCrucibleRecipe(transmuteFuel, canFuel, Forestry.canEmpty, fuelAspectList);

    public static final ResearchPage transmuteOilTextPage = new ResearchPage(transmuteOilText);
    public static final ResearchPage bucketOilRecipePage = new ResearchPage(bucketOilRecipe);
    public static final ResearchPage waxCapsuleOilRecipePage = new ResearchPage(waxCapsuleOilRecipe);
    public static final ResearchPage refractoryOilRecipePage = new ResearchPage(refractoryOilRecipe);
    public static final ResearchPage canOilRecipePage = new ResearchPage(canOilRecipe);
    public static final ResearchPage[] transmuteOilPages = {transmuteOilTextPage, bucketOilRecipePage};
    public static final ResearchPage[] transmuteOilPagesForestry = {transmuteOilTextPage, bucketOilRecipePage, waxCapsuleOilRecipePage, refractoryOilRecipePage, canOilRecipePage};

    public static final ResearchPage transmuteFuelTextPage = new ResearchPage(transmuteFuelText);
    public static final ResearchPage bucketFuelRecipePage = new ResearchPage(bucketFuelRecipe);
    public static final ResearchPage waxCapsuleFuelRecipePage = new ResearchPage(waxCapsuleFuelRecipe);
    public static final ResearchPage refractoryFuelRecipePage = new ResearchPage(refractoryFuelRecipe);
    public static final ResearchPage canFuelRecipePage = new ResearchPage(canFuelRecipe);
    public static final ResearchPage[] transmuteFuelPages = {transmuteFuelTextPage, bucketFuelRecipePage};
    public static final ResearchPage[] transmuteFuelPagesForestry = {transmuteFuelTextPage, bucketFuelRecipePage, waxCapsuleFuelRecipePage, refractoryFuelRecipePage, canFuelRecipePage};
}
