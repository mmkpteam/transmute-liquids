package com.mrpenguin.transmuteliquids.lib;

/**
 * Copyright (c) 2014 MrPenguin
 * All rights reserved.
 *
 * This program and the accompanying materials are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 *
 * This class was made by MrPenguin and is distributed as a part of the Transmute Liquids mod.
 * Transmute Liquids is a derivative work on Thaumcraft 4 (c) Azanor 2012.
 * http://www.minecraftforum.net/topic/2011841-thaumcraft-41114-updated-2052014/
 */

import thaumcraft.api.ThaumcraftApi;
import thaumcraft.api.aspects.Aspect;
import thaumcraft.api.aspects.AspectList;
import thaumcraft.api.crafting.CrucibleRecipe;
import thaumcraft.api.research.ResearchPage;

import cpw.mods.fml.common.registry.GameRegistry;

import net.minecraft.item.ItemStack;

public class Forestry {

    public static final String Forestry = "Forestry";

    public static final String transmuteWaterWaxCapsule = "transmuteWaterWaxCapsule";
    public static final String transmuteWaterRefractory = "transmuteWaterRefractory";
    public static final String transmuteWaterCan = "transmuteWaterCan";

    public static final String transmuteLavaRefractory = "transmuteLavaRefractory";
    public static final String transmuteLavaCan = "transmuteLavaCan";

    public static final String transmuteHoneyPot = "transmuteHoneyPot";
    public static final String transmuteHoneyWaxCapsule = "transmuteHoneyWaxCapsule";
    public static final String transmuteHoneyRefractory = "transmuteHoneyRefractory";
    public static final String transmuteHoneyCan = "transmuteHoneyCan";

    public static final String transmuteJuiceWaxCapsule = "transmuteJuiceWaxCapsule";
    public static final String transmuteJuiceRefractory = "transmuteJuiceRefractory";
    public static final String transmuteJuiceCan = "transmuteJuiceCan";

    public static final String transmuteSeedOilWaxCapsule = "transmuteSeedOilWaxCapsule";
    public static final String transmuteSeedOilRefractory = "transmuteSeedOilRefractory";
    public static final String transmuteSeedOilCan = "transmuteSeedOilCan";

    public static final String transmuteBiomassWaxCapsule = "transmuteBiomassWaxCapsule";
    public static final String transmuteBiomassRefractory = "transmuteBiomassRefractory";
    public static final String transmuteBiomassCan = "transmuteBiomassCan";

    public static final String transmuteBiofuelWaxCapsule = "transmuteBiofuelWaxCapsule";
    public static final String transmuteBiofuelRefractory = "transmuteBiofuelRefractory";
    public static final String transmuteBiofuelCan = "transmuteBiofuelCan";

    public static final String transmuteHoney = "transmuteHoney";
    public static final String transmuteJuice = "transmuteJuice";
    public static final String transmuteSeedOil = "transmuteSeedOil";
    public static final String transmuteBiomass = "transmuteBiomass";
    public static final String transmuteBiofuel = "transmuteBiofuel";

    public static final String transmuteHoneyText = "transmuteHoneyText";
    public static final String transmuteJuiceText = "transmuteJuiceText";
    public static final String transmuteSeedOilText = "transmuteSeedOilText";
    public static final String transmuteBiomassText = "transmuteBiomassText";
    public static final String transmuteBiofuelText = "transmuteBiofuelText";

    public static final ItemStack waxCapsule = GameRegistry.findItemStack(Forestry, "waxCapsule", 1);
    public static final ItemStack refractoryEmpty = GameRegistry.findItemStack(Forestry, "refractoryEmpty", 1);
    public static final ItemStack canEmpty = GameRegistry.findItemStack(Forestry, "canEmpty", 1);

    public static final ItemStack waxCapsuleWater = GameRegistry.findItemStack(Forestry, "waxCapsuleWater", 1);
    public static final ItemStack refractoryWater = GameRegistry.findItemStack(Forestry, "refractoryWater", 1);
    public static final ItemStack canWater = GameRegistry.findItemStack(Forestry, "waterCan", 1);

    public static final ItemStack refractoryLava = GameRegistry.findItemStack(Forestry, "refractoryLava", 1);
    public static final ItemStack canLava = GameRegistry.findItemStack(Forestry, "canLava", 1);

    public static final ItemStack potHoney = GameRegistry.findItemStack(Forestry, "honeyPot", 1);
    public static final ItemStack waxCapsuleHoney = GameRegistry.findItemStack(Forestry, "waxCapsuleHoney", 1);
    public static final ItemStack refractoryHoney = GameRegistry.findItemStack(Forestry, "refractoryHoney", 1);
    public static final ItemStack canHoney = GameRegistry.findItemStack(Forestry, "canHoney", 1);

    public static final ItemStack waxCapsuleJuice = GameRegistry.findItemStack(Forestry, "waxCapsuleJuice", 1);
    public static final ItemStack refractoryJuice = GameRegistry.findItemStack(Forestry, "refractoryJuice", 1);
    public static final ItemStack canJuice = GameRegistry.findItemStack(Forestry, "canJuice", 1);

    public static final ItemStack waxCapsuleSeedOil = GameRegistry.findItemStack(Forestry, "waxCapsuleSeedOil", 1);
    public static final ItemStack refractorySeedOil = GameRegistry.findItemStack(Forestry, "refractorySeedOil", 1);
    public static final ItemStack canSeedOil = GameRegistry.findItemStack(Forestry, "canSeedOil", 1);

    public static final ItemStack bucketBiomass = GameRegistry.findItemStack(Forestry, "bucketBiomass", 1);
    public static final ItemStack waxCapsuleBiomass = GameRegistry.findItemStack(Forestry, "waxCapsuleBiomass", 1);
    public static final ItemStack refractoryBiomass = GameRegistry.findItemStack(Forestry, "refractoryBiomass", 1);
    public static final ItemStack canBiomass = GameRegistry.findItemStack(Forestry, "biomassCan", 1);

    public static final ItemStack bucketBiofuel = GameRegistry.findItemStack(Forestry, "bucketBiofuel", 1);
    public static final ItemStack waxCapsuleBiofuel = GameRegistry.findItemStack(Forestry, "waxCapsuleBiofuel", 1);
    public static final ItemStack refractoryBiofuel = GameRegistry.findItemStack(Forestry, "refractoryBiofuel", 1);
    public static final ItemStack canBiofuel = GameRegistry.findItemStack(Forestry, "biofuelCan", 1);

    public static final AspectList honeyAspectList1 = new AspectList().add(Aspect.CROP, 4).add(Aspect.EXCHANGE, 3);
    public static final AspectList honeyAspectList2 = new AspectList().add(Aspect.CROP, 6).add(Aspect.EXCHANGE, 3);
    public static final AspectList juiceAspectList = new AspectList().add(Aspect.CROP, 6).add(Aspect.EXCHANGE, 3);
    public static final AspectList seedOilAspectList = new AspectList().add(Aspect.PLANT, 6).add(Aspect.EXCHANGE, 3);
    public static final AspectList biomassAspectList = new AspectList().add(Aspect.PLANT, 6).add(Aspect.TOOL, 4).add(Aspect.EXCHANGE, 3);
    public static final AspectList biofuelAspectList = new AspectList().add(Aspect.PLANT, 6).add(Aspect.TOOL, 6).add(Aspect.EXCHANGE, 3);

    public static final CrucibleRecipe waxCapsuleWaterRecipe = ThaumcraftApi.addCrucibleRecipe(TransmuteLiquids.transmuteLiquids, waxCapsuleWater, waxCapsule, TransmuteLiquids.waterAspectList);
    public static final CrucibleRecipe refractoryWaterRecipe = ThaumcraftApi.addCrucibleRecipe(TransmuteLiquids.transmuteLiquids, refractoryWater, refractoryEmpty, TransmuteLiquids.waterAspectList);
    public static final CrucibleRecipe canWaterRecipe = ThaumcraftApi.addCrucibleRecipe(TransmuteLiquids.transmuteLiquids, canWater, canEmpty, TransmuteLiquids.waterAspectList);

    public static final CrucibleRecipe refractoryLavaRecipe = ThaumcraftApi.addCrucibleRecipe(TransmuteLiquids.transmuteLiquids, refractoryLava, refractoryEmpty, TransmuteLiquids.lavaAspectList);
    public static final CrucibleRecipe canLavaRecipe = ThaumcraftApi.addCrucibleRecipe(TransmuteLiquids.transmuteLiquids, canLava, canEmpty, TransmuteLiquids.lavaAspectList);

    public static final CrucibleRecipe potHoneyRecipe = ThaumcraftApi.addCrucibleRecipe(TransmuteLiquids.transmuteLiquids, potHoney, waxCapsule, honeyAspectList1);
    public static final CrucibleRecipe waxCapsuleHoneyRecipe = ThaumcraftApi.addCrucibleRecipe(TransmuteLiquids.transmuteLiquids, waxCapsuleHoney, waxCapsule, honeyAspectList2);
    public static final CrucibleRecipe refractoryHoneyRecipe = ThaumcraftApi.addCrucibleRecipe(TransmuteLiquids.transmuteLiquids, refractoryHoney, refractoryEmpty, honeyAspectList2);
    public static final CrucibleRecipe canHoneyRecipe = ThaumcraftApi.addCrucibleRecipe(TransmuteLiquids.transmuteLiquids, canHoney, canEmpty, honeyAspectList2);

    public static final CrucibleRecipe waxCapsuleJuiceRecipe = ThaumcraftApi.addCrucibleRecipe(TransmuteLiquids.transmuteLiquids, waxCapsuleJuice, waxCapsule, juiceAspectList);
    public static final CrucibleRecipe refractoryJuiceRecipe = ThaumcraftApi.addCrucibleRecipe(TransmuteLiquids.transmuteLiquids, refractoryJuice, refractoryEmpty, juiceAspectList);
    public static final CrucibleRecipe canJuiceRecipe = ThaumcraftApi.addCrucibleRecipe(TransmuteLiquids.transmuteLiquids, canJuice, canEmpty, juiceAspectList);

    public static final CrucibleRecipe waxCapsuleSeedOilRecipe = ThaumcraftApi.addCrucibleRecipe(TransmuteLiquids.transmuteLiquids, waxCapsuleSeedOil, waxCapsule, seedOilAspectList);
    public static final CrucibleRecipe refractorySeedOilRecipe = ThaumcraftApi.addCrucibleRecipe(TransmuteLiquids.transmuteLiquids, refractorySeedOil, refractoryEmpty, seedOilAspectList);
    public static final CrucibleRecipe canSeedOilRecipe = ThaumcraftApi.addCrucibleRecipe(TransmuteLiquids.transmuteLiquids, canSeedOil, canEmpty, seedOilAspectList);

    public static final CrucibleRecipe bucketBiomassRecipe = ThaumcraftApi.addCrucibleRecipe(TransmuteLiquids.transmuteLiquids, bucketBiomass, TransmuteLiquids.bucket, biomassAspectList);
    public static final CrucibleRecipe waxCapsuleBiomassRecipe = ThaumcraftApi.addCrucibleRecipe(TransmuteLiquids.transmuteLiquids, waxCapsuleBiomass, waxCapsule, biomassAspectList);
    public static final CrucibleRecipe refractoryBiomassRecipe = ThaumcraftApi.addCrucibleRecipe(TransmuteLiquids.transmuteLiquids, refractoryBiomass, refractoryEmpty, biomassAspectList);
    public static final CrucibleRecipe canBiomassRecipe = ThaumcraftApi.addCrucibleRecipe(TransmuteLiquids.transmuteLiquids, canBiomass, canEmpty, biomassAspectList);

    public static final CrucibleRecipe bucketBiofuelRecipe = ThaumcraftApi.addCrucibleRecipe(TransmuteLiquids.transmuteLiquids, bucketBiofuel, TransmuteLiquids.bucket, biofuelAspectList);
    public static final CrucibleRecipe waxCapsuleBiofuelRecipe = ThaumcraftApi.addCrucibleRecipe(TransmuteLiquids.transmuteLiquids, waxCapsuleBiofuel, waxCapsule, biofuelAspectList);
    public static final CrucibleRecipe refractoryBiofuelRecipe = ThaumcraftApi.addCrucibleRecipe(TransmuteLiquids.transmuteLiquids, refractoryBiofuel, refractoryEmpty, biofuelAspectList);
    public static final CrucibleRecipe canBiofuelRecipe = ThaumcraftApi.addCrucibleRecipe(TransmuteLiquids.transmuteLiquids, canBiofuel, canEmpty, biofuelAspectList);

    public static final ResearchPage waxCapsuleWaterRecipePage = new ResearchPage(waxCapsuleWaterRecipe);
    public static final ResearchPage refractoryWaterRecipePage = new ResearchPage(refractoryWaterRecipe);
    public static final ResearchPage canWaterRecipePage = new ResearchPage(canWaterRecipe);
    public static final ResearchPage[] transmuteWaterPages = {TransmuteLiquids.waterTextPage, TransmuteLiquids.bucketWaterRecipePage, waxCapsuleWaterRecipePage, refractoryWaterRecipePage, canWaterRecipePage};

    public static final ResearchPage refractoryLavaRecipePage = new ResearchPage(refractoryLavaRecipe);
    public static final ResearchPage canLavaRecipePage = new ResearchPage(canLavaRecipe);
    public static final ResearchPage[] transmuteLavaPages = {TransmuteLiquids.lavaTextPage, TransmuteLiquids.bucketLavaRecipePage, refractoryLavaRecipePage, canLavaRecipePage};

    public static final ResearchPage transmuteHoneyTextPage = new ResearchPage(transmuteHoneyText);
    public static final ResearchPage potHoneyRecipePage = new ResearchPage(potHoneyRecipe);
    public static final ResearchPage waxCapsuleHoneyRecipePage = new ResearchPage(waxCapsuleHoneyRecipe);
    public static final ResearchPage refractoryHoneyRecipePage = new ResearchPage(refractoryHoneyRecipe);
    public static final ResearchPage canHoneyRecipePage = new ResearchPage(canHoneyRecipe);
    public static final ResearchPage[] transmuteHoneyPages = {transmuteHoneyTextPage, potHoneyRecipePage, waxCapsuleHoneyRecipePage, refractoryHoneyRecipePage, canHoneyRecipePage};

    public static final ResearchPage transmuteJuiceTextPage = new ResearchPage(transmuteJuiceText);
    public static final ResearchPage waxCapsuleJuiceRecipePage = new ResearchPage(waxCapsuleJuiceRecipe);
    public static final ResearchPage refractoryJuiceRecipePage = new ResearchPage(refractoryJuiceRecipe);
    public static final ResearchPage canJuiceRecipePage = new ResearchPage(canJuiceRecipe);
    public static final ResearchPage[] transmuteJuicePages = {transmuteJuiceTextPage, waxCapsuleJuiceRecipePage, refractoryJuiceRecipePage, canJuiceRecipePage};

    public static final ResearchPage transmuteSeedOilTextPage = new ResearchPage(transmuteSeedOilText);
    public static final ResearchPage waxCapsuleSeedOilRecipePage = new ResearchPage(waxCapsuleSeedOilRecipe);
    public static final ResearchPage refractorySeedOilRecipePage = new ResearchPage(refractorySeedOilRecipe);
    public static final ResearchPage canSeedOilRecipePage = new ResearchPage(canSeedOilRecipe);
    public static final ResearchPage[] transmuteSeedOilPages = {transmuteSeedOilTextPage, waxCapsuleSeedOilRecipePage, refractorySeedOilRecipePage, canSeedOilRecipePage};

    public static final ResearchPage transmuteBiomassTextPage = new ResearchPage(transmuteBiomassText);
    public static final ResearchPage bucketBiomassRecipePage = new ResearchPage(bucketBiomassRecipe);
    public static final ResearchPage waxCapsuleBiomassRecipePage = new ResearchPage(waxCapsuleBiomassRecipe);
    public static final ResearchPage refractoryBiomassRecipePage = new ResearchPage(refractoryBiomassRecipe);
    public static final ResearchPage canBiomassRecipePage = new ResearchPage(canBiomassRecipe);
    public static final ResearchPage[] transmuteBiomassPages = {transmuteBiomassTextPage, bucketBiomassRecipePage, waxCapsuleBiomassRecipePage, refractoryBiomassRecipePage, canBiomassRecipePage};

    public static final ResearchPage transmuteBiofuelTextPage = new ResearchPage(transmuteBiofuelText);
    public static final ResearchPage bucketBiofuelRecipePage = new ResearchPage(bucketBiofuelRecipe);
    public static final ResearchPage waxCapsuleBiofuelRecipePage = new ResearchPage(waxCapsuleBiofuelRecipe);
    public static final ResearchPage refractoryBiofuelRecipePage = new ResearchPage(refractoryBiofuelRecipe);
    public static final ResearchPage canBiofuelRecipePage = new ResearchPage(canBiofuelRecipe);
    public static final ResearchPage[] transmuteBiofuelPages = {transmuteBiofuelTextPage, bucketBiofuelRecipePage, waxCapsuleBiofuelRecipePage, refractoryBiofuelRecipePage, canBiofuelRecipePage};
}