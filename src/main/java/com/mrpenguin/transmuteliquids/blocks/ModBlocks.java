package com.mrpenguin.transmuteliquids.blocks;

import com.mrpenguin.transmuteliquids.lib.Reference;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.StatCollector;

/**
 * Copyright (c) 2014 MrPenguin
 * All rights reserved.
 *
 * This program and the accompanying materials are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 *
 * This class was made by MrPenguin and is distributed as a part of the Transmute Liquids mod.
 * Transmute Liquids is a derivative work on Thaumcraft 4 (c) Azanor 2012.
 * http://www.minecraftforum.net/topic/2011841-thaumcraft-41114-updated-2052014/
 */

public enum ModBlocks {

    ;

    private static boolean registered = false;
    public final Block block;
    private final String internalName;
    private final Class<? extends ItemBlock> itemBlockClass;
    private final CreativeTabs creativeTabs;

    ModBlocks(String internalName, Block block) {
        this(internalName, block, ItemBlock.class, null);
    }

    ModBlocks(String internalName, Block block, CreativeTabs creativeTabs) {
        this(internalName, block, ItemBlock.class, creativeTabs);
    }

    ModBlocks(String internalName, Block block, Class<? extends ItemBlock> itemBlockClass){
        this(internalName, block, itemBlockClass, null);
    }

    ModBlocks(String internalName, Block block, Class<? extends ItemBlock> itemBlockClass, CreativeTabs creativeTabs) {
        this.internalName = internalName;
        this.block = block;
        this.itemBlockClass = itemBlockClass;
        this.creativeTabs = creativeTabs;
        block.setBlockName(Reference.MOD_ID + "." + internalName);
    }

    public static void registerAll() {
        if (registered) {
            return;
        }
        for (ModBlocks b : ModBlocks.values()) {
            b.register();
        }
        registered = true;
    }

    public String getInternalName() {
        return internalName;
    }

    public String getStatName() {
        return StatCollector.translateToLocal(block.getUnlocalizedName()).replace("tile.", "block.");
    }

    private void register() {
        GameRegistry.registerBlock(block.setCreativeTab(creativeTabs).setBlockTextureName(Reference.MOD_ID + ":" + internalName), itemBlockClass, Reference.MOD_ID + "-block." + internalName);
    }
}
