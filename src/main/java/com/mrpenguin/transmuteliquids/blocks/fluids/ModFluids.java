package com.mrpenguin.transmuteliquids.blocks.fluids;

/**
 * Copyright (c) 2014 MrPenguin
 * All rights reserved.
 *
 * This program and the accompanying materials are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 *
 * This class was made by MrPenguin and is distributed as a part of the Transmute Liquids mod.
 * Transmute Liquids is a derivative work on Thaumcraft 4 (c) Azanor 2012.
 * http://www.minecraftforum.net/topic/2011841-thaumcraft-41114-updated-2052014/
 */

import com.mrpenguin.transmuteliquids.lib.Reference;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.StatCollector;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;

public class ModFluids {

    public static Fluid moltenAirShard;
    public static Fluid moltenFireShard;
    public static Fluid moltenWaterShard;
    public static Fluid moltenEarthShard;
    public static Fluid moltenOrderShard;
    public static Fluid moltenEntropyShard;

    public static Block moltenAirShardBlock;
    public static Block moltenFireShardBlock;
    public static Block moltenWaterShardBlock;
    public static Block moltenEarthShardBlock;
    public static Block moltenOrderShardBlock;
    public static Block moltenEntropyShardBlock;

    public static void registerFluids() {
        moltenAirShard = new Fluid("moltenAirShard");
        FluidRegistry.registerFluid(moltenAirShard);
        moltenAirShardBlock = new BlockMoltenAirShard(moltenAirShard, Material.lava).setBlockName("moltenAirShardBlock");
        GameRegistry.registerBlock(moltenAirShardBlock, Reference.MOD_ID + "_" + moltenAirShardBlock.getUnlocalizedName().substring(5));
        moltenAirShard.setUnlocalizedName(moltenAirShardBlock.getUnlocalizedName());

        moltenFireShard = new Fluid("moltenFireShard");
        FluidRegistry.registerFluid(moltenFireShard);
        moltenFireShardBlock = new BlockMoltenFireShard(moltenFireShard, Material.lava).setBlockName("moltenFireShardBlock");
        GameRegistry.registerBlock(moltenFireShardBlock, Reference.MOD_ID + "_" + moltenFireShardBlock.getUnlocalizedName().substring(5));
        moltenFireShard.setUnlocalizedName(moltenFireShardBlock.getUnlocalizedName());

        moltenWaterShard = new Fluid("moltenWaterShard");
        FluidRegistry.registerFluid(moltenWaterShard);
        moltenWaterShardBlock = new BlockMoltenWaterShard(moltenWaterShard, Material.lava).setBlockName("moltenWaterShardBlock");
        GameRegistry.registerBlock(moltenWaterShardBlock, Reference.MOD_ID + "_" + moltenWaterShardBlock.getUnlocalizedName().substring(5));
        moltenWaterShard.setUnlocalizedName(moltenWaterShardBlock.getUnlocalizedName());

        moltenEarthShard = new Fluid("moltenEarthShard");
        FluidRegistry.registerFluid(moltenEarthShard);
        moltenEarthShardBlock = new BlockMoltenEarthShard(moltenEarthShard, Material.lava).setBlockName("moltenEarthShardBlock");
        GameRegistry.registerBlock(moltenEarthShardBlock, Reference.MOD_ID + "_" + moltenEarthShardBlock.getUnlocalizedName().substring(5));
        moltenEarthShard.setUnlocalizedName(moltenEarthShardBlock.getUnlocalizedName());

        moltenOrderShard = new Fluid("moltenOrderShard");
        FluidRegistry.registerFluid(moltenOrderShard);
        moltenOrderShardBlock = new BlockMoltenOrderShard(moltenOrderShard, Material.lava).setBlockName("moltenOrderShardBlock");
        GameRegistry.registerBlock(moltenOrderShardBlock, Reference.MOD_ID + "_" + moltenOrderShardBlock.getUnlocalizedName().substring(5));
        moltenOrderShard.setUnlocalizedName(moltenOrderShardBlock.getUnlocalizedName());

        moltenEntropyShard = new Fluid("moltenEntropyShard");
        FluidRegistry.registerFluid(moltenEntropyShard);
        moltenEntropyShardBlock = new BlockMoltenEntropyShard(moltenEntropyShard, Material.lava).setBlockName("moltenEntropyShardBlock");
        GameRegistry.registerBlock(moltenEntropyShardBlock, Reference.MOD_ID + "_" + moltenEntropyShardBlock.getUnlocalizedName().substring(5));
        moltenEntropyShard.setUnlocalizedName(moltenEntropyShardBlock.getUnlocalizedName());
    }

    //Will be improving and implementing this design later on
    /**public static boolean registered = false;
    public final Block block;
    public final Fluid fluid;
    private final String internalName;
    private final CreativeTabs creativeTabs;

    Fluids(String internalName, Block block, Fluid fluid, int luminosity, int density, int temperature, int viscosity, boolean gaseous) {
        this(internalName, block, fluid, null, luminosity, density, temperature, viscosity, gaseous);
    }

    Fluids(String internalName, Block block, Fluid fluid, CreativeTabs creativeTabs, int luminosity, int density, int temperature, int viscosity, boolean gaseous) {
        this.internalName = internalName;
        this.block = block;
        this.fluid = fluid;
        this.creativeTabs = creativeTabs;
        block.setBlockName(internalName);
        fluid.setLuminosity(luminosity);
        fluid.setDensity(density);
        fluid.setTemperature(temperature);
        fluid.setViscosity(viscosity);
        fluid.setGaseous(gaseous);
        fluid.setUnlocalizedName(internalName);
    }

    public static void registerAll() {
        if(registered) {
            return;
        }

        for(Fluids f : Fluids.values()) {
            f.register();
        }
    }

    public String getInternalName() {
        return internalName;
    }

    public String getStatName() {
        return StatCollector.translateToLocal(fluid.getUnlocalizedName().replace("tile.", "fluid."));
    }

    private void register() {
        FluidRegistry.registerFluid(fluid);
        GameRegistry.registerBlock(block, "fluid." + internalName);
    }**/
}
