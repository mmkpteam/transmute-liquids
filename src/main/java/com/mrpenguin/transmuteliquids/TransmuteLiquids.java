package com.mrpenguin.transmuteliquids;

/**
 * Copyright (c) 2014 MrPenguin
 * All rights reserved.
 *
 * This program and the accompanying materials are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 *
 * This class was made by MrPenguin and is distributed as a part of the Transmute Liquids mod.
 * Transmute Liquids is a derivative work on Thaumcraft 4 (c) Azanor 2012.
 * http://www.minecraftforum.net/topic/2011841-thaumcraft-41114-updated-2052014/
 */

import com.mrpenguin.transmuteliquids.blocks.fluids.ModFluids;
import com.mrpenguin.transmuteliquids.handlers.BucketHandler;
import com.mrpenguin.transmuteliquids.items.ModItems;
import com.mrpenguin.transmuteliquids.lib.Reference;
import com.mrpenguin.transmuteliquids.proxy.IProxy;

import com.mrpenguin.transmuteliquids.research.ThaumonomiconResearch;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.common.MinecraftForge;

@Mod(modid = Reference.MOD_ID, name = Reference.MOD_NAME, version = Reference.MOD_VERSION, dependencies = Reference.MOD_DEPENDENCIES)
public class TransmuteLiquids {

    @Instance
    public static TransmuteLiquids instance;

    @SidedProxy(clientSide = Reference.CLIENT_PROXY_CLASS, serverSide = Reference.SERVER_PROXY_CLASS)
    public static IProxy proxy;

    @EventHandler
    public static void preInit(FMLPreInitializationEvent event) {

        ModFluids.registerFluids();
        ModItems.registerItems();

        System.out.println("[TL] Transmute Liquids : Successful PreInitialization");
    }

    @EventHandler
    public static void init(FMLInitializationEvent event) {

        BucketHandler.INSTANCE.buckets.put(ModFluids.moltenAirShardBlock, ModItems.moltenAirShardBucket);
        BucketHandler.INSTANCE.buckets.put(ModFluids.moltenFireShardBlock, ModItems.moltenFireShardBucket);
        BucketHandler.INSTANCE.buckets.put(ModFluids.moltenWaterShardBlock, ModItems.moltenWaterShardBucket);
        BucketHandler.INSTANCE.buckets.put(ModFluids.moltenEarthShardBlock, ModItems.moltenEarthShardBucket);
        BucketHandler.INSTANCE.buckets.put(ModFluids.moltenOrderShardBlock, ModItems.moltenOrderShardBucket);
        BucketHandler.INSTANCE.buckets.put(ModFluids.moltenEntropyShardBlock, ModItems.moltenEntropyShardBucket);
        MinecraftForge.EVENT_BUS.register(BucketHandler.INSTANCE);

        System.out.println("[TL] Transmute Liquids : Successful Initialization");
    }

    @EventHandler
    public static void postInit(FMLPostInitializationEvent event) {


        ThaumonomiconResearch.addResearchTab();
        ThaumonomiconResearch.addResearch();

        System.out.println("[TL] Transmute Liquids : Successful PostInitialization");
    }

}
